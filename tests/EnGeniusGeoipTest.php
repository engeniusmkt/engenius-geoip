<?php
declare(strict_types=1);
namespace TestNamespace;

use PHPUnit\Framework\TestCase;
use phpmock\mockery\PHPMockery;
use EnGenius\EnGeniusGeoip;

class DetectionPluginNotLoadedTest extends TestCase
{

    protected function tearDown(): void {
        \Mockery::close();
    }
    
    public function testReturnTrueIfFunctionUndefined() {
        $mock = PHPMockery::mock('EnGenius', "function_exists")->andReturn(false);
        $mock->with('geoip_detect2_get_info_from_current_ip')->once();

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $this->assertTrue($geoip->detection_plugin_not_loaded());
    }

    public function testReturnFalseIfDetectionFunctionDefined() {
        $mock = PHPMockery::mock('EnGenius', "function_exists")->andReturn(true);
        $mock->with('geoip_detect2_get_info_from_current_ip')->once();

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $this->assertFalse($geoip->detection_plugin_not_loaded());
    }
    
}


class HideNotificationTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testReturnTrueIfCookieSet() {
        $mock = PHPMockery::mock('EnGenius', "setcookie")->andReturn(true);

        unset($_COOKIE['engenius_location_cookie']);
        unset($_SERVER['disable_geoip']);
        
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();
        $geoip->shouldReceive('visitor_at_correct_domain')->andReturn(false);

        $_COOKIE['engenius_location_cookie'] = 'some_value';
        
        $this->assertTrue($geoip->hide_notification());
    }

    public function testReturnTrueAndSetCookieIfDisableGeoipSet() {
        unset($_COOKIE['engenius_location_cookie']);
        unset($_SERVER['disable_geoip']);
        $_SERVER['disable_geoip'] = true;

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();
        $geoip->shouldReceive('visitor_at_correct_domain')->andReturn(false);
        $geoip->shouldReceive('cookie_time')->andReturn(10000000);

        $geoip->browser_language = 'en';

        $mock = PHPMockery::mock('EnGenius', "setcookie")->with('engenius_location_cookie', 'en', 10000000);
        
        $this->assertTrue($geoip->hide_notification());
    }

    public function testReturnIsCorrectSiteLanguageResultTrue() {
        unset($_COOKIE['engenius_location_cookie']);
        unset($_SERVER['disable_geoip']);
        unset($_SERVER['SERVER_NAME']);

        $_SERVER['SERVER_NAME'] = 'www.engeniustech.com';

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();
        $geoip->shouldReceive('visitor_at_correct_domain')->with($_SERVER['SERVER_NAME'])->andReturn(true);
        $geoip->shouldReceive('is_correct_site_language')->andReturn(true);
        
        $this->assertTrue($geoip->hide_notification());

        \Mockery::close();
    }

    public function testReturnIsCorrectSiteLanguageResultFalse() {
        unset($_COOKIE['engenius_location_cookie']);
        unset($_SERVER['disable_geoip']);

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();
        $geoip->shouldReceive('visitor_at_correct_domain')->andReturn(true);
        $geoip->shouldReceive('is_correct_site_language')->andReturn(false);
        
        $this->assertFalse($geoip->hide_notification());

        \Mockery::close();
    }
    
    public function testReturnFalseIfNoCriteriaMet() {
        $mock = PHPMockery::mock('EnGenius', "setcookie")->andReturn(true);
    
        unset($_COOKIE['engenius_location_cookie']);
        unset($_SERVER['disable_geoip']);

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();
        $geoip->shouldReceive('visitor_at_correct_domain')->andReturn(false);

        $this->assertFalse($geoip->hide_notification());
    }

}


class VisitorAtCorrectDomainTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testExceptionIfContinentUnset() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = null;

        $this->expectException('Exception');
        $this->expectExceptionMessage("Continent is undefined");

        $geoip->visitor_at_correct_domain('www.engeniustech.com.tw');
    }

    public function testNorthAmerica() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'NA';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniustech.com'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniusnetworks.eu'));
    }

    public function testSouthAmerica() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'SA';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniustech.com'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniusnetworks.eu'));
    }

    public function testAntarctica() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'AN';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniustech.com'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniustech.com.sg'));
    }

    public function testEurope() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniusnetworks.eu'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniustech.com'));
    }

    public function testAfrica() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'AF';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniustech.com.sg'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniustech.com'));
    }

    public function testAsia() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'AS';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniustech.com.sg'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniustech.com'));
    }

    public function testOceania() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'OC';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniustech.com.sg'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniustech.com'));
    }

    public function testTaiwan() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'AS';
        $geoip->country = 'TAIWAN';
        
        $this->assertTrue($geoip->visitor_at_correct_domain('www.engeniustech.com.tw'));
        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniustech.com.sg'));
    }

    public function testNoMatch() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'NO_MATCH';

        $this->assertFalse($geoip->visitor_at_correct_domain('www.engeniustech.com.sg'));
    }

}


class IsCorrectSiteLanguageTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testUrlLanguagePrefixIsSpanish() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();
        $geoip->shouldReceive('get_url_language_prefix')->andReturn('es');

        $geoip->browser_language = 'es';

        $this->assertTrue($geoip->is_correct_site_language());

        $geoip->browser_language = 'en';

        $this->assertFalse($geoip->is_correct_site_language());
    }

    public function testDefault() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();
        $geoip->shouldReceive('get_url_language_prefix')->andReturn('');

        $geoip->browser_language = 'es';

        $this->assertFalse($geoip->is_correct_site_language());

        $geoip->browser_language = 'en';

        $this->assertTrue($geoip->is_correct_site_language());
    }

    public function testICLLanguageCode() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        define('ICL_LANGUAGE_CODE', 'ES-ME');

        $geoip->browser_language = 'es';

        $this->assertTrue($geoip->is_correct_site_language());

        $geoip->browser_language = 'en';

        $this->assertFalse($geoip->is_correct_site_language());
    }

}


class GetIpInfoTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testGetResultFromDetectionPlugin() {
        $mock = PHPMockery::mock('EnGenius', "geoip_detect2_get_info_from_current_ip")->andReturn(array(
            'continent' => 'AF',
            'country' => 'SOUTH_AFRICA'
        ));
        $mock->with(array('en'), array());

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $this->assertEquals($geoip->get_ip_info(), array(
            'continent' => 'AF',
            'country' => 'SOUTH_AFRICA'
        ));
    }

}

class GetUrlLanguagePrefixTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testSimplePrefix() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        unset($_SERVER['REQUEST_URI']);

        $_SERVER['REQUEST_URI'] = '/es';

        $this->assertEquals($geoip->get_url_language_prefix(), 'es');
    }

    public function testComplexPrefix() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        unset($_SERVER['REQUEST_URI']);
        
        $_SERVER['REQUEST_URI'] = '/es/some_other.php';

        $this->assertEquals($geoip->get_url_language_prefix(), 'es');
    }

}

class Country {
    public $names;
    public $isoCode = 'US';

    function __construct($names) {
        $this->names = $names;
    }
}

class Continent {
    public $name = 'North America';
    public $code = 'NA';

    function __construct() {}
}

class Info {
    public $country;

    function __construct($country) {
        $this->country = $country;
    }
}


class GetCountryTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testExtractCountryFromInfo() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $info = new Info(new Country(array('en' => 'TAIWAN', 'es' => 'El Taiwan')));

        $geoip->info = $info;

        $this->assertEquals($geoip->get_country(), 'TAIWAN');
    }
}


class GotoRegionTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testExceptionIfContinentUnset() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = null;

        $this->expectException('Exception');
        $this->expectExceptionMessage("Continent is undefined");

        $geoip->goto_region('EU', 'DE');
    }

    public function testExceptionIfCountryUnset() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';

        $this->expectException('Exception');
        $this->expectExceptionMessage("Country is undefined");

        $geoip->goto_region();
    }

    public function testGermany() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'DE';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (DE)',
            'link' => 'https://www.engeniusnetworks.eu/de/?disable_geoip'
        ));
    }

    public function testFrance() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'FR';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (FR)',
            'link' => 'https://www.engeniusnetworks.eu/fr/?disable_geoip'
        ));
    }

    public function testSpain() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'ES';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (ES)',
            'link' => 'https://www.engeniusnetworks.eu/es/?disable_geoip'
        ));
    }

    public function testItaly() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'IT';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (IT)',
            'link' => 'https://www.engeniusnetworks.eu/it/?disable_geoip'
        ));
    }

    public function testUnitedKingdom() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'GB';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (United Kingdom)',
            'link' => 'https://www.engeniusnetworks.eu/gb/?disable_geoip'
        ));
    }

    public function testRussia() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'RU';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (RU)',
            'link' => 'https://www.engeniusnetworks.eu/ru/?disable_geoip'
        ));
    }

    public function testRomania() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'RO';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (RO)',
            'link' => 'https://www.engeniusnetworks.eu/ro/?disable_geoip'
        ));
    }

    public function testPoland() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'PL';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (PL)',
            'link' => 'https://www.engeniusnetworks.eu/pl/?disable_geoip'
        ));
    }

    public function testSweden() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'SV';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (SV)',
            'link' => 'https://www.engeniusnetworks.eu/sv/?disable_geoip'
        ));
    }

    public function testNetherlands() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'NL';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (NL)',
            'link' => 'https://www.engeniusnetworks.eu/nl/?disable_geoip'
        ));
    }

    public function testUkraine() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'UK';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius (Ukraine)',
            'link' => 'https://www.engeniusnetworks.eu/uk/?disable_geoip'
        ));
    }

    public function testEuropeDefault() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = '_';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius EU',
            'link' => 'https://www.engeniusnetworks.eu/?disable_geoip'
        ));
    }


    public function testNorthAmericaEnglish() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'NA';
        $geoip->isoCode = '_';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius Americas (EN)',
            'link' => 'https://www.engeniustech.com/?disable_geoip'
        ));
    }

    public function testNorthAmericaSpanish() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'NA';
        $geoip->isoCode = '_';

        $geoip->browser_language = 'ES';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius Americas (ES)',
            'link' => 'https://www.engeniustech.com/es/?disable_geoip'
        ));
    }


    public function testSouthAmericaEnglish() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'SA';
        $geoip->isoCode = '_';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius Americas (EN)',
            'link' => 'https://www.engeniustech.com/?disable_geoip'
        ));
    }

    public function testSouthAmericaSpanish() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'SA';
        $geoip->isoCode = '_';

        $geoip->browser_language = 'ES';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius Americas (ES)',
            'link' => 'https://www.engeniustech.com/es/?disable_geoip'
        ));
    }

    public function testAntarcticaEnglish() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'AN';
        $geoip->isoCode = '_';

        $this->assertEquals($geoip->goto_region(), array(
            'label' => 'EnGenius Americas (EN)',
            'link' => 'https://www.engeniustech.com/?disable_geoip'
        ));
    }
}


class GetFlagTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testGermany() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'DE';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/de.png");
    }

    public function testFrance() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'FR';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/fr.png");
    }

    public function testSpain() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'ES';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/es.png");
    }

    public function testItaly() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'IT';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/it.png");
    }

    public function testUnitedKingdom() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'GB';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/gb.png");
    }

    public function testRussia() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'RU';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/ru.png");
    }

    public function testRomania() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'RO';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/ro.png");
    }

    public function testPoland() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'PL';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/pl.png");
    }

    public function testSweden() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'SV';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/sv.png");
    }

    public function testNetherlands() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'NL';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/nl.png");
    }

    public function testUkraine() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->isoCode = 'UK';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/uk.png");
    }

    public function testUnitedStates() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'NA';
        $geoip->isoCode = 'US';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/us.png");
    }

    public function testEuropeDefault() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'EU';
        $geoip->isoCode = 'AL';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/flags/eu.jpg");
    }

    public function testGlobalDefault() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->continent = 'NA';
        $geoip->isoCode = 'ME';

        $this->assertEquals($geoip->get_flag(), "/wp-content/plugins/engenius-geoip/src/img/globe.svg");
    }

}



class GetBrowserLanguageTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testFrench() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        unset($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'fr-CH, de;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5';

        $this->assertEquals($geoip->get_browser_language(), "fr");
    }

    public function testChinese() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        unset($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'ZH-CN';

        $this->assertEquals($geoip->get_browser_language(), "zh");
    }

    
    public function testEnglishAsFallback() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        unset($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'pr-us,en,fr;q=0.5';

        $this->assertEquals($geoip->get_browser_language(), "en");
    }

    public function testFrenchAsFallback() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        unset($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'pr-us,in,fr;q=0.5';

        $this->assertEquals($geoip->get_browser_language(), "fr");
    }

    public function testDefault() {
        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        unset($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'pr-us,in;q=0.5';

        $this->assertEquals($geoip->get_browser_language(), "en");
    }

}


class MainTest extends TestCase {

    protected function tearDown(): void {
        \Mockery::close();
    }

    public function testMainNoDetectionPlugin() {
        $info = new Info(new Country(array('en' => 'TAIWAN', 'es' => 'El Taiwan')));
        $info->continent = new Continent();

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->shouldReceive('detection_plugin_not_loaded')->andReturns(true)->once();

        $geoip->shouldReceive('load_assets')->times(0);
        $geoip->shouldReceive('render')->times(0);

        $this->assertEquals($geoip->main(), null);
    }

    public function testMainHideNotification() {
        $info = new Info(new Country(array('en' => 'TAIWAN', 'es' => 'El Taiwan')));
        $info->continent = new Continent();

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->shouldReceive('detection_plugin_not_loaded')->andReturns(false)->once();

        $geoip->shouldReceive('get_ip_info')->andReturns($info)->once();
        $geoip->shouldReceive('get_country')->andReturns('TAIWAN')->once();
        $geoip->shouldReceive('get_browser_language')->andReturns('en')->once();
        $geoip->shouldReceive('get_flag')->andReturns('/wp-content/plugins/engenius-geoip/flags/us.png')->once();
        
        $geoip->shouldReceive('hide_notification')->andReturns(true)->once();

        $geoip->shouldReceive('load_assets')->times(0);
        $geoip->shouldReceive('render')->times(0);

        $this->assertEquals($geoip->main(), null);
    }


    public function testMain() {
        $info = new Info(new Country(array('en' => 'TAIWAN', 'es' => 'El Taiwan')));
        $info->continent = new Continent();

        $region = array(
            'label' => 'EnGenius Americas (EN)',
            'link' => 'https://www.engeniustech.com/?disable_geoip'
        );

        $geoip = \Mockery::mock('EnGenius\EnGeniusGeoip')->makePartial();

        $geoip->shouldReceive('detection_plugin_not_loaded')->andReturns(false)->once();
        $geoip->shouldReceive('get_ip_info')->andReturns($info)->once();
        $geoip->shouldReceive('get_country')->andReturns('TAIWAN')->once();
        $geoip->shouldReceive('get_browser_language')->andReturns('en')->once();
        $geoip->shouldReceive('get_flag')->andReturns('/wp-content/plugins/engenius-geoip/flags/us.png')->once();
        $geoip->shouldReceive('hide_notification')->andReturns(false)->once();

        $geoip->shouldReceive('get_welcome_message')->andReturns('message')->once();
        $geoip->shouldReceive('get_region_selection_message')->andReturns('region_selection')->once();
        $geoip->shouldReceive('get_region_message')->andReturns('change_region')->once();
        $geoip->shouldReceive('goto_message')->andReturns('goto')->once();
        $geoip->shouldReceive('goto_region')->andReturns($region)->once();

        $geoip->shouldReceive('load_assets')->once();
        $geoip->shouldReceive('render')->once();

        $this->assertEquals($geoip->main(), null);

        $this->assertEquals($geoip->info, $info);
        $this->assertEquals($geoip->continent, $info->continent->code);
        $this->assertEquals($geoip->country, 'TAIWAN');
        $this->assertEquals($geoip->browser_language, 'en');
        $this->assertEquals($geoip->isoCode, $info->country->isoCode);
        $this->assertEquals($geoip->flag, '/wp-content/plugins/engenius-geoip/flags/us.png');
        $this->assertEquals($geoip->welcome_message, 'message');
        $this->assertEquals($geoip->region_selection_message, 'region_selection');
        $this->assertEquals($geoip->region_message, 'change_region');
        $this->assertEquals($geoip->goto, 'goto');
        $this->assertEquals($geoip->region_label, $region['label']);
        $this->assertEquals($geoip->region_link, $region['link']);
        $this->assertEquals($geoip->delete_icon, '/wp-content/plugins/engenius-geoip/src/img/delete.svg');

    }

}
