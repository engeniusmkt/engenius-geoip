<?php

/**
* Plugin Name: EnGenius GEOIP
* Author: Alec Jones
* Description: Custom plugin by Alec Jones, will provide a IP-based notification to guide visitors to other EnGenius sites. The Geoip-Detection plugin is a required dependency of this plugin.
* Text Domain: engenius-geoip
**/
    require __DIR__ . '/src/EnGeniusGeoip.php';
    require __DIR__ . '/src/main.php';