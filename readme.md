Steps for installation.

1. Please make sure to disable the old Wordpress Geoip plugin, which forces redirection. (WP GeoIP Country Redirect, by Crivion).
2. Git clone this repository into your Wordpress plugins folder.
3. This plugin has the Geolocation IP detection plugin as its dependency. You must install this as well, or the plugin will not activate and simply fail silently.
https://wordpress.org/plugins/geoip-detect/
4. Even if the plugin doesn't seem to appear, it's probably running. By default, it will not show if the visitor is in the EnGenius Americas website, and their IP matches to an IP in either North or South America.
5. The design of the plugin can be modified by modifying the EnGeniusGeoip class's load_assets() method. By default, it will load style.css and style_la.css (if LA site). Feel free to remove these assets and substitute your own files to change the design.
6. For the purposes of testing out a new design, it would be best to set up the plugin to run on a test page. This can by adding a condition that the plugin will only run if the request matches a specific path.