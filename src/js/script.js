jQuery(document).ready(function() {
  function setCookie(name, value, days) {
    // Setting cookie that will persist for the user's session.
    document.cookie = name + "=" + value + ";path=/;";
  }

  function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

  var locationHeaderContainer = jQuery("#location-header-container");

  jQuery(".close-location-header").click(function() {
    locationHeaderContainer.addClass("close");
  });

  locationHeaderContainer.find(".location-header-confirm").click(function() {
    setCookie(
      "engenius_location_cookie",
      locationHeaderContainer.attr("data-language")
    );
    locationHeaderContainer.addClass("close");
  });

  var locationCookieValue = getCookie("engenius_location_cookie");
  if (locationCookieValue) {
    var currentLanguage = locationHeaderContainer.attr("data-language");
    if (
      locationCookieValue == "all" ||
      locationCookieValue == currentLanguage
    ) {
      return;
    }
  }

  var selectElem = jQuery("#engenius-geoip-select").select2();
  var geoipIsOpen = false;

  jQuery("#engenius-geoip-select").on("select2:select", function(e) {
    var link = e.params.data.id;
    window.location.replace(link);
  });

  jQuery("#engenius-geoip-select").on("select2:select", function(e) {
    var link = e.params.data.id;
    window.location.replace(link);
  });

  jQuery("#open-select2-mobile").click(function() {
    var elem = jQuery(this);
    if (geoipIsOpen) {
      geoipIsOpen = false;
      selectElem.select2("close");
      elem.children(".chevron").addClass("bottom");
    } else {
      geoipIsOpen = true;
      selectElem.select2("open");
      elem.children(".chevron").removeClass("bottom");
    }
  });

  jQuery("#location-header-container .close-geoip").click(function() {
    locationHeaderContainer.addClass("close");
  });

  if (Headroom) {
    try {
      var headroom = new Headroom(
        document.getElementById("location-header-container"),
        {
          offset: 0
        }
      );
      // initialise
      headroom.init();
    } catch (e) {
      console.log("Headroom error:");
      console.log(e);
    }
  }

  setTimeout(function() {
    locationHeaderContainer.removeClass("close");
  }, 4000);
});
